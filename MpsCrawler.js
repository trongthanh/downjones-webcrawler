/*jshint laxbreak:true*/
var Crawler = require('crawler').Crawler,
    fs = require('fs');

var MpsCrawler = function (){
    var me = this,
        //data extracting RegExp
        regTotal = /T&#7893;ng s&#7889; (\d*) b&#224;i vi&#7871;t&nbsp;\/&nbsp;(\d*) trang./g,
        regName = /(?:Truy nã(?: đối tượng)?|đối tượng|bị can):?\s*(.*?)(?:\n|<|")/i,
        regYOB = /(?:Năm sinh|SN):?\s*(.*?)(?:\s|<|,)/i,
        regAdd = /(?:Nơi ĐKTT|(?:Nơi đăng )?HKTT):?\s*(.*?)(?:\n|<|\.)/i,
        regId = /vcmsviewcontent\/GbkG\/2015\/2015\/(\d*)/i,
        cacheFile = 'data/cache.json',
        pageData = {},
        counter = 0,
        cache = null,
        //*
        url = 'http://mps.gov.vn/c/portal/render_portlet?p_l_id=13515&p_p_id=vcmsviewcontent_INSTANCE_GbkG&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-2&p_p_col_pos=0&p_p_col_count=1&currentURL=%2Fweb%2Fguest%2Fct_trangchu%2F-%2Fvcmsviewcontent%2FGbkG%2F2015%2F2015&_vcmsviewcontent_INSTANCE_GbkG_categoryId=2015&_vcmsviewcontent_INSTANCE_GbkG_struts_action=%2Fvcmsviewcontent%2Fview&_vcmsviewcontent_INSTANCE_GbkG_cat_parent=2015',
         //build the URL
        pageUrl = 'http://mps.gov.vn/c/portal/render_portlet?'
            + 'p_l_id=13515&'
            + 'p_p_id=vcmsviewcontent_INSTANCE_GbkG&'
            + 'p_p_lifecycle=0&'
            + 'p_p_state=normal&'
            + 'p_p_mode=view&'
            + 'p_p_col_id=column-2&'
            + 'p_p_col_pos=0&'
            + 'p_p_col_count=1&'
            //+ 'currentURL=%2Fweb%2Fguest%2Fct_trangchu%2F-%2Fvcmsviewcontent%2FGbkG%2F2015%2F2015&'
            + '_vcmsviewcontent_INSTANCE_GbkG_struts_action=%2Fvcmsviewcontent%2Fview&'
            + '_vcmsviewcontent_INSTANCE_GbkG_page=$page$&'
            + '_vcmsviewcontent_INSTANCE_GbkG_publishMonth=9&'
            //+ '_vcmsviewcontent_INSTANCE_GbkG_cmd=&'
            //+ '_vcmsviewcontent_INSTANCE_GbkG_publishDay=28&'
            + '_vcmsviewcontent_INSTANCE_GbkG_categoryId=2015&',
            //+ '_vcmsviewcontent_INSTANCE_GbkG_publishYear=2012';
        /*/
            url = 'http://localhost:3000/mock/page-01.html',
            pageUrl = 'http://localhost:3000/mock/page-0$page$.html',
        //*/
        c = new Crawler({
            maxConnections: 10,
            timeout: 10000,
            retries: 3,
            retryTimeout: 2000,

            // This will be called for each crawled page
            callback: function(error, result, $) {
                console.log('Error: ' + error);

                // $ is a jQuery instance scoped to the server-side DOM of the page
                //console.log($('td').text());
                //console.log(result.body);

                var total = regTotal.exec(result.body),
                    $articles = $('.smallpage .group1, .smallpage .group1_last'),
                    items = [],
                    lastTotal = 0, lastPageTotal = 0;

                console.log('total: ' + total);
                //write last page for debuggin error
                fs.writeFile('data/lastpage.html', result.body);
                //console.log(cache);
                if (total && !isNaN(total[2])) {
                    me.pageTotal = total[2];
                    me.total = total[1];
                }
                if (cache) {
                    lastTotal = cache.total;
                    lastPageTotal = cache.pageTotal;
                }

                $articles.each(function (idx, el){
                    var $el = $(el),
                        str = $el.html(),
                        nameres = regName.exec(str),
                        yobres = regYOB.exec(str),
                        addres = regAdd.exec(str),
                        idres = regId.exec(str),
                        $img = $el.find('img'),
                        id, status;

                    counter ++;
                    //check for cache
                    id = (idres)? idres[1] : '';
                    if (cache && id && cache.itemsMap[id]) {
                        status = 'unchanged';
                    } else {
                        status = 'new';
                    }

                    items.push({
                        num: me.currentPage + ' - ' + counter,
                        id: id,
                        photo: ($img.length)? 'http://mps.gov.vn/' + $img.attr('src') : '',
                        name: (nameres)? nameres[1] : '',
                        yob: (yobres)? yobres[1] : '',
                        add: (addres)? addres[1] : '',
                        status: status,
                        raw: $el.text().replace(/\n[\n\s]+/g, '<br>').replace(/\s+/g, ' ')
                    });
                });

                var dataObj = {
                    total: me.total,
                    page: me.currentPage,
                    pageTotal: me.pageTotal,
                    lastTotal: lastTotal,
                    lastPageTotal: lastPageTotal,
                    items: items
                };

                pageData[me.currentPage] = dataObj;

                //if all pages are loaded:
                if (me.currentPage >= me.pageTotal) {
                    var allArr = [],
                        itemsMap = {},
                        cacheMap,
                        data, i, il, delItem;
                    for (i = 1; i <= me.pageTotal; i++) {
                        allArr = allArr.concat(pageData[i].items);
                    }

                    for (i = 0, il = allArr.length; i < il; i++) {
                        itemsMap[allArr[i].id] = allArr[i];
                    }

                    //check for deleted items
                    if (cache) {
                        cacheMap = cache.itemsMap;
                        for (var prop in cacheMap) {
                            if (!itemsMap[prop]) {
                                delItem = cacheMap[prop];
                                delItem.status = 'deleted';
                                items.push(delItem);
                            }
                        }
                    }

                    data = {
                        total: me.total,
                        pageTotal: me.pageTotal,
                        itemsMap: itemsMap
                    };

                    fs.writeFile(cacheFile, JSON.stringify(data), function (err) {
                        if (err) throw err;
                        console.log('Cache file written!');
                    });
                    //store cache for the session
                    cache = data;

                }

                //return data
                me.callback(dataObj);


            }
        });

    fs.readFile(cacheFile, function (err, data) {
        if (err) {
            console.log('Cannot read cache file, let it be empty');
        } else {
            cache = JSON.parse(data);
        }
    });

        //*/
    this.c = c;
    this.url = url;
    this.pageUrl = pageUrl;
    this.pageData = pageData;

    console.log('MpsCrawler ready');
};

MpsCrawler.prototype = {
    c: null,
    url: '',
    pageUrl: '',
    callback: null,
    pageData: null,
    pageTotal: NaN,
    total: 0,

    loadPage: function (page, callback) {
        var pageData = this.pageData,
            crawler = this.c,
            pageTotal = this.pageTotal;

        console.log('Loading page ' + page + ' of ' + pageTotal);
        if (pageTotal && page > pageTotal) {
            callback({
                total: 0,
                pageTotal: pageTotal,
                page: page,
                items: null
            });
            return;
        }

        if (pageData[page]) {
            callback(pageData[page]);
        } else {
            this.callback = callback;
            this.currentPage = page;
            if (parseInt(page, 10) === 1) {
                crawler.queue(this.url);
            } else {
                crawler.queue(this.pageUrl.replace(/\$page\$/, page));
            }
        }

    }
};



//c.queue([{
    //'html': '<td align="right">T&#7893;ng s&#7889; 343 b&#224;i vi&#7871;t&nbsp;/&nbsp;35 trang.</td>'
//}]);

module.exports = new MpsCrawler();