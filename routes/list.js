/*
 * © 2012
 * @author Thanh Tran
 */
var crawler = require('../MpsCrawler');

exports.mps = function (req, res){
    var page = req.query.page  || 1;
    page = parseInt(page, 10);

    crawler.loadPage(page, function (data){
        var items = data.items,
            arr = [], item, status;
        //res.render('list', {
            //title: 'MPS Crawler',
            //total: data.total,
            //items: data.items
        //});

        if (items) {
            //normalize and return jQuery's data table compatible array
            for (var i = 0, il = items.length; i < il; i++) {
                item = items[i];
                status = item.status;
                if (status === 'new') {
                    status = '<span class="new-status">New</span>';
                } else if (status === 'deleted') {
                    status = '<span class="deleted-status">Deleted</span>';
                } else {
                    status = '';
                }

                arr[i] = [
                    '<input type="checkbox" class="checkbox">',
                    '<div class="ui-icon ui-icon-plus ir">+</div>',
                    item.num, //order no.
                    item.id, //article id
                    (item.photo)? '<img src="' + item.photo + '" height=120>' : '',
                    item.name,
                    item.yob,
                    item.add,
                    status,
                    item.raw
                ];
            }
        }
        res.json({
            pageTotal: data.pageTotal,
            total: data.total,
            page: data.page,
            lastTotal: data.lastTotal,
            lastPageTotal: data.lastPageTotal,
            aaData: arr
        });
    });

};