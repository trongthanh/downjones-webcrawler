/*
 * Load mock data, immitate service
 */ 

var fs = require('fs');

// var loadDataHandler;

/**
 * Handle fs.readFile callback
 **/
/*
function _fileReadHandler(err, data) {
    if(err) {
        console.error("Could not open file: %s", err);
        process.exit(1);
    }
    loadDataHandler(data);
}*/


function loadData() {
    return fs.readFileSync('mock/page-01.html', 'utf8');
}

module.exports = {
    loadData: loadData
};