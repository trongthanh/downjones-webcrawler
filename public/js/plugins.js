/*jshint noarg:true, eqeqeq:true, bitwise:true, undef:true, curly:true, browser:true, maxerr:50, mootools:false, jquery:true*/
/*global localStorage, console*/

// Avoid `console` errors in browsers that lack a console.
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

// Place any jQuery/helper plugins in here.

/*
 * classList.js: Cross-browser full element.classList implementation.
 * 2011-06-15
 *
 * By Eli Grey, http://eligrey.com
 * Public Domain.
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
 */

/*global self, document, DOMException */

/*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js*/

if (typeof document !== "undefined" && !("classList" in document.createElement("a"))) {

(function (view) {

"use strict";

var
      classListProp = "classList"
    , protoProp = "prototype"
    , elemCtrProto = (view.HTMLElement || view.Element)[protoProp]
    , objCtr = Object
    , strTrim = String[protoProp].trim || function () {
        return this.replace(/^\s+|\s+$/g, "");
    }
    , arrIndexOf = Array[protoProp].indexOf || function (item) {
        var
              i = 0
            , len = this.length
        ;
        for (; i < len; i++) {
            if (i in this && this[i] === item) {
                return i;
            }
        }
        return -1;
    }
    // Vendors: please allow content code to instantiate DOMExceptions
    , DOMEx = function (type, message) {
        this.name = type;
        this.code = DOMException[type];
        this.message = message;
    }
    , checkTokenAndGetIndex = function (classList, token) {
        if (token === "") {
            throw new DOMEx(
                  "SYNTAX_ERR"
                , "An invalid or illegal string was specified"
            );
        }
        if (/\s/.test(token)) {
            throw new DOMEx(
                  "INVALID_CHARACTER_ERR"
                , "String contains an invalid character"
            );
        }
        return arrIndexOf.call(classList, token);
    }
    , ClassList = function (elem) {
        var
              trimmedClasses = strTrim.call(elem.className)
            , classes = trimmedClasses ? trimmedClasses.split(/\s+/) : []
            , i = 0
            , len = classes.length
        ;
        for (; i < len; i++) {
            this.push(classes[i]);
        }
        this._updateClassName = function () {
            elem.className = this.toString();
        };
    }
    , classListProto = ClassList[protoProp] = []
    , classListGetter = function () {
        return new ClassList(this);
    }
;
// Most DOMException implementations don't allow calling DOMException's toString()
// on non-DOMExceptions. Error's toString() is sufficient here.
DOMEx[protoProp] = Error[protoProp];
classListProto.item = function (i) {
    return this[i] || null;
};
classListProto.contains = function (token) {
    token += "";
    return checkTokenAndGetIndex(this, token) !== -1;
};
classListProto.add = function (token) {
    token += "";
    if (checkTokenAndGetIndex(this, token) === -1) {
        this.push(token);
        this._updateClassName();
    }
};
classListProto.remove = function (token) {
    token += "";
    var index = checkTokenAndGetIndex(this, token);
    if (index !== -1) {
        this.splice(index, 1);
        this._updateClassName();
    }
};
classListProto.toggle = function (token) {
    token += "";
    if (checkTokenAndGetIndex(this, token) === -1) {
        this.add(token);
    } else {
        this.remove(token);
    }
};
classListProto.toString = function () {
    return this.join(" ");
};

if (objCtr.defineProperty) {
    var classListPropDesc = {
          get: classListGetter
        , enumerable: true
        , configurable: true
    };
    try {
        objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
    } catch (ex) { // IE 8 doesn't support enumerable:true
        if (ex.number === -0x7FF5EC54) {
            classListPropDesc.enumerable = false;
            objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
        }
    }
} else if (objCtr[protoProp].__defineGetter__) {
    elemCtrProto.__defineGetter__(classListProp, classListGetter);
}

}(self));

}

/**
 * Random Utility
 */

var Random = (function () {
    /**
     * Return a integer value within min - max (inclusive)
     */
    var getRandomInt = function (min, max) {
        return min + Math.floor(Math.random() * (max - min + 1));
    };

    /**
     * Get a random sequence
     */
    var getRandomSequence = function (min, max) {
        var o = [], //original seq
            r = [], //result
            c, //child item
            l; //length
        if (min > max) {
            var tmp = max;
            max = min;
            min = tmp;
        }

        l = Math.abs(max - min) + 1;
        //original sequence
        for (var i = 0; i < l; i ++) {
          o[i] = min + i;
          //log(o[i]);
        }

        while (true) {
            l = o.length;
            if (l > 0) {
                c = o.splice(getRandomInt(0,l - 1),1)[0];
                r.push(c);
            } else {
                break;
            }
        }

        return r;
    };

    //expose public functions
    return {
        getRandomInt: getRandomInt,
        getRandomSequence: getRandomSequence
    };
})();

(function ($){
    $.fn.animateLoadingDotDot = function (str) {

        return this.each(function () {
            var $this = $(this),
                dotReg = /([\.\+\-\#\=])/g,
                html,
                $dots,
                idx = -1,
                len,
                intervalid;

            if (!str) {
                //use existing text if no text is passed
                str = $this.text();
            }

            //clear last interval
            if ($this.data('intervalid')) {
                clearInterval($this.data('intervalid'));
                $this.data('intervalid', undefined);
            }

            if (dotReg.test(str)) {
                html = str.replace(dotReg, '<span class="dot" style="display: none">$1</span>');
            } else {
                console.log('no dot to animate');
                $this.html(str);
                return;
            }

            $this.html(html);
            $dots = $this.find('.dot');
            len = $dots.length;

            intervalid = setInterval(function () {
                if (idx >= len) {
                    idx = -1;
                    //after a cycle, check if the dots are still there
                    if ($dots.parent().length <= 0) {
                        clearInterval(intervalid);
                        $this.data('intervalid', undefined);
                        return;
                    }
                    //console.log($dots.parent());
                }

                for (var i = 0; i < len; i++) {
                    if (i <= idx) {
                        $dots.eq(i).css('display', 'inline');
                    } else {
                        $dots.eq(i).css('display', 'none');
                    }
                }

                idx ++;
            }, 200);

            $this.data('intervalid', intervalid);
        });
    };

})(jQuery);