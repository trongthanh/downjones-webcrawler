/**
 * @author: Thanh Tran
 */
/*jshint noarg:true, eqeqeq:true, bitwise:true, undef:true, curly:true, browser:true, maxerr:50, mootools:false, jquery:true*/
/*global console*/

(function ($) {
    var grid,
        serviceurl = '/mps';

    $(document).ready(function () {
        grid = $('#data-grid').dataTable( {
            //'sAjaxSource': 'http://localhost:3000/mps',
            'bPaginate': false,
            'bJQueryUI': true,
            'aaSorting': [ /*[1,'asc']*/ ],
            //'aaData': data,
            'aoColumns': [
                { 'sTitle': '' },
                { 'sTitle': '' },
                { 'sTitle': 'No.' },
                { 'sTitle': 'ID' },
                { 'sTitle': 'Photo' },
                { 'sTitle': 'Name' },
                { 'sTitle': 'YOB' },
                { 'sTitle': 'Address' },
                { 'sTitle': 'Status' },
                { 'sTitle': 'Raw' }
            ],
            'aoColumnDefs': [
                { 'sClass': 'check-col', 'aTargets': [0]},
                { 'sClass': 'icon-col', 'aTargets': [1]},
                { 'sClass': 'num-col', 'aTargets': [2]},
                { 'sClass': 'narrow-col', 'aTargets': [3, 6, 8]},
                { 'sClass': 'photo-col', 'aTargets': [4] },
                { 'bVisible': false, 'aTargets': [9]} //hide raw column
            ]
        });

        $('#data-grid tbody td .ui-icon').live( 'click', function () {
            var $this = $(this),
                tr = $this.parents('tr')[0];

            if (grid.fnIsOpen(tr)) {
                /* This row is already open - close it */
                $this.removeClass('ui-icon-minus').addClass('ui-icon-plus');
                grid.fnClose(tr);
            } else {
                /* Open this row */
                var details = grid.fnGetData(tr);
                $this.removeClass('ui-icon-plus').addClass('ui-icon-minus');
                grid.fnOpen( tr, details[9], 'raw small' );
            }
        } );

        $('#data-grid .check-col').live('click', function (e) {
            if (e.target.tagName === 'INPUT') {
                //let the checkbox check itself
                return;
            }
            var $checkBox = $(this).find('input[type="checkbox"]');
            $checkBox.attr('checked', !$checkBox.attr('checked'));
        });



        function loadPage(page){
            //load pages
            $.ajax(serviceurl, {
                data: {page: page},
                dataType: 'json',
                success: function (data){
                    var page = parseInt(data.page, 10),
                        pageTotal = parseInt(data.pageTotal, 10);
                    console.log('load page success ' + data.page );

                    grid.fnAddData(data.aaData);
                    if (page === 1) {
                        $('#statistics').html(
                            'Records: <strong>' + data.total + '</strong> ' +
                            '(Last: ' + data.lastTotal + ') ' +
                            '/ Pages: <strong>' + data.pageTotal + '</strong> ' +
                            '(Last: ' + data.lastPageTotal + ')'
                        );
                    }

                    if (page < pageTotal) {
                        $('#status').animateLoadingDotDot('Pages loaded: ' + page + ' of ' + pageTotal + '.....');

                        //avoid flooding the mps server
                        setTimeout(function (){
                            loadPage(page + 1);
                        }, 1000);


                    } else {
                        $('#status').html('All ' + pageTotal + ' pages loaded');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#status').html('Service error! Status:' + textStatus + ', Server Message: ' + errorThrown);
                }

            });
        }

        $('#status').animateLoadingDotDot();
        $('.dataTables_empty').html('Loading first page...');

        loadPage(1);

    });


})(jQuery);
